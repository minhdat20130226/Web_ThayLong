const footer = document.querySelector("footer#footer");
footer.innerHTML =
	`

	<div class="container ">
		<div class="row row-md ">
			<div class="col-xl-7">
				<div class="row row-md">
					<div class="col-xl-4 ">
						<a href="/src/views/policy.html" title="" >Chính sách bảo mật</a>
					</div>
					<div class="col-xl-4">
						<a href="#" title="">Quy chế hoạt động</a>
					</div>
					<div class="col-xl-4">
						<a href="#" title="">Kiểm tra hóa đơn điện tử</a>
					</div>
					<div class="col-xl-4">
						<a href="#" title="">Tra cứu thông tin bảo hành</a>
					</div>
					<div class="col-xl-4">
						<a href="#" title="">Tin tuyển dụng</a>
					</div>
					<div class="col-xl-4">
						<a href="#" title="">Tin khuyến mãi</a>
					</div>
					<div class="col-xl-4">
						<a href="#" title="">Hướng dẫn mua online</a>
					</div>
					<div class="col-xl-4">
						<a href="#" title="">Hướng dẫn mua trả góp</a>
					</div>
					<div class="col-xl-4">
						<a href="#" title="">Chính sách trả góp</a>
					</div>
					<div class="col-xl-4">
						<a href="#" title="">Hệ thống cửa hàng</a>
					</div>
					<div class="col-xl-4">
						<a href="#" title="">Hệ thống bảo hành</a>
					</div>
					<div class="col-xl-4">
						<a href="#" title="">Giới thiệu máy đổi trả</a>
					</div>
					<div class="col-xl-4">
						<a href="#" title="">Chính sách đổi trả</a>
					</div>
				</div>
			</div>
			<div class="col-xl-5">
				<div class="row row-cols-xl-2">
					<div class="col  ">
						<a href="#" title="">Tư vấn mua hàng (Miễn phí)
							<br>
							<span>18006601</span>
						</a>
					</div>
					<div class="col ">
						<a href="#" title="">
							Góp ý , khiếu nại dịch vụ(8h00 - 22h00)
							<br>
							<span>18006616</span>
						</a>
					</div>

				</div>
					<div class="row row-cols-xl-2">
				<div class="col ">
						<a href="#" title="">
							Hỗ trợ thanh toán :
							<br>
							<img src="/src/images/company.PNG" alt="">
						</a>
					</div>
					<div class="col">
						<a href="#" title="">
							Chứng nhận :
							<br>
							<img src="/src/images/notification1.PNG" alt="" class="img-fluid w-100">
						</a>
					</div>
					</div>
			</div>
		</div>
	</div>
<div class="footer-mobile d-block d-sm-none">
	<table class="table table-bordered">
		<tbody>
			<tr>
				<td>
					<a href="#" title="" class="text-dark">Tư vấn mua hàng</a>
					<div>
						<i class="fa fa-phone" aria-hidden="true"></i>
						1800 6601
					</div>
				</td>
				<td>
					<a href="#" title="" class="text-dark">Góp ý khiếu nại</a>
					<div>
						<i class="fa fa-phone" aria-hidden="true"></i>
						1800 6616
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<a href="#" title="" class="text-dark">Thông tin khác</a>
					<i class="fa fa-chevron-down" aria-hidden="true"></i>
				</td>
				<td>
					<a href="#" title="" class="text-dark">Cửa hàng FPT Shop</a>
					<i class="fa fa-chevron-right" aria-hidden="true"></i>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="fixed-action-btn smooth-scroll" style="bottom: 45px; right: 24px;">
			<a href="#header" class="btn-floating btn-large red">
			  <i class="fa fa-arrow-up"></i>
			</a>
		  </div><!--end footer mobile-->

        `
